Task 9
===

This task has 5 excersises (or subtasks)


Excersise 1 (replcace)
---

Replace symbol " by '.


Excersise 2 (palindrome)
---

Check is given string a palindrome or not.


Excersise 3 (shortest word)
---

Get the shortest word in the string. The word can contain any symbols except 
whitespaces (` `, `\n`, `\t` and so on). 

Excersise 4
---

Implement a brunch of functions which receive a changeable number of strings 
and return next parameters:  


1) characters that appear in all strings;  
2) characters that appear in at least one string;  
3) characters that appear at least in two strings;  
4) characters of alphabet, that were not used in any string.


Excersise 5 (number of letters)

Implement a function, that takes string as an argument and returns a 
dictionary, that contains letters of given string as keys and number of their 
occurrence as values.  