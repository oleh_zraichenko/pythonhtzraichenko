# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 19:47:25 2020

@author: catri
"""


def is_palindrome(string):
    return string == string[::-1]


if __name__ == '__main__':
    s = input('Please, input the string:\n')
    print(is_palindrome(s))