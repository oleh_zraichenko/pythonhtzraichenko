def replace(s: str)->str:
    for i in range(len(s)):
        if s[i] == '"':
            s = s[:i] + "'" + s[i+1:]
    return s

if __name__ == '__main__':
    words = 'Loook " at me " now'
    print(replace(words))