# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 11:55:50 2020

@author: catri
"""

def count_letters(s: str):
    dict = {}
    for ch in s:
        dict[ch] = s.count(ch)
        
    return dict

if __name__ == '__main__':
    print(count_letters('samplestring'))