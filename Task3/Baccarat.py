import sys
from random import randint


CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

def baccarat(soul, *picked_cards):
    def check_point(n):
        if n in [str(i) for i in range(2,10)]:
            return int(n)
        elif n == "A":
            return 1
        else:
            return 0

    res = 0
    for card in picked_cards:
        if card in CARDS:
            res += check_point(card)
            res %= 10
        else:
            return 'Do not cheat'

    if res <= 5 and soul == 'p':
        add_card = CARDS[radint(0, len(CARDS)-1)]
        print(f'Your result is {res}\nGiving 3rd card to the player ->', add_card)
        res += check_point(add_card)
    elif res <= 4 and soul == 'd':
        add_card = CARDS[randint(0, len(CARDS) - 1)]
        print(f'Dealer result is {res}\nGiving 3rd card to the dealer ->', add_card)
        res += check_point(add_card)
    return res % 10


if __name__ == '__main__':
    controller = input('Do you want to play y/n\n')
    while controller != 'n':
        x_p = CARDS[randint(0, len(CARDS)-1)]
        x_d = CARDS[randint(0, len(CARDS)-1)]
        y_p = CARDS[randint(0, len(CARDS)-1)]
        y_d = CARDS[randint(0, len(CARDS)-1)]
        print('Your cards:', x_p, y_p)
        res_player = baccarat('p', x_p, y_p)
        res_dealer = baccarat('d', x_d, y_d)
        print(f'{res_player} > {res_player} You win') if res_player > res_dealer \
            else print(f'{res_player} < {res_player} You loose!') if res_player < res_dealer \
            else print(f'{res_player} = {res_player} Tie!')
        controller = input('Continue playing? y/n\n')

