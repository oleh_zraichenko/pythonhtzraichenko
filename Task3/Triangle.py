import sys
import math


def validate_point(x: float, y: float):
    if (y < 1 and y > abs(x)):
        return True
    else:
        return False

if __name__ == '__main__':
    x = float(input('Enter your x:'))
    y = float(input('Enter your y:'))
    print('Is this point in this area:', validate_point(x, y))