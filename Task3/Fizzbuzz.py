import sys


def fizzbus(a):
    num = ""
    if a % 3 == 0:
        num += "fizz"
    if a % 5 == 0:
        num += "buzz"
    if num == "":
        num += str(a)
    return num

if __name__ == '__main__':
    a = int(input())
    if a > 100 or a < 0:
        print('Oops! Your input is out of range')
    else:
        print(fizzbus(a))
