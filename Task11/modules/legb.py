a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        #To call a variable from global scope I changed line above print to this
        #global a
        #To call a variable from eclosing_function I just commented thiis line
        #a = "I am local variable"
        #Also we can change this line to 
        #nonlocal a
        print(a)

    inner_function()