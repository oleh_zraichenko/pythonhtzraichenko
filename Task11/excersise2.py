def remember_result(func):
    last_call = None
    def wrapper(*args, **kwargs):
        nonlocal last_call
        print(f"Last result = '{last_call}'")
        last_call = func(*args, **kwargs)
    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
        
    print(f"Current result = '{result}'")
    return result

sum_list('a', 'c')
