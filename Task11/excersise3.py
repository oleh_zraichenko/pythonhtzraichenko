def call_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
    wrapper.has_run = False
    return wrapper
       


@call_once
def sum_of_numbers(a, b):
    return a + b

if __name__ == "__main__":
    print(sum_of_numbers(4, 10))
    print(sum_of_numbers(4, 19))