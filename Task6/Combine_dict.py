import collections


def combine_dict(*args):
    counter = collections.Counter()
    for d in args:
        counter.update(d)
    result = dict(counter)
    return result

if __name__ == '__main__':
    dict1 = {'a': 100, 'b': 200}
    dict2 = {'a': 200, 'c': 100}
    dict3 = {'b': 100, 'c': 300}
    print(combine_dict(dict1, dict2))
    print(combine_dict(dict1, dict2, dict3))