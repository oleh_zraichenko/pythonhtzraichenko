class Rectangle:
    def __init__(self, a, b = 5):
        self.__sideA = a
        self.__sideB = b

    def getSideA(self):
        return self.__sideA

    def getSideB(self):
        return self.__sideB

    def area(self):
        return self.__sideB * self.__sideA

    def perimeter(self):
        return (self.__sideB + self.__sideA) * 2

    def isSquare(self):
        return True if self.__sideA == self.__sideB else False

    def replaceSides(self):
        self.__sideA += self.__sideB
        self.__sideB = self.__sideA - self.__sideB
        self.__sideA -= self.__sideB


