import Rectangle

class ArrayRectangles:
    __rectangle_array = []

    @classmethod
    def setNumberOfRect(cls, n : int):
        obj = cls()
        for i in range(n):
            obj.__rectangle_array.append(None)
        return obj

    @classmethod
    def setArrayOfRect(cls, *args : Rectangle):
        obj = cls()
        obj.__rectangle_array.append(args)
        return obj

    def addRectangle(self, rect : Rectangle):
        self.__rectangle_array.append(rect)
