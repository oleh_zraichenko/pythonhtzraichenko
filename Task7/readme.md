Task 7
===

This task has 2 excersises

Excersise 1
---

In this excersise 2 classes were created: Rectangle and ArrayRectanles. ArrayRectangles istance contains array of Rectangle instance. The Rectangle instance contains information about two sides of rectangle (sideA and sideB). Rectangle allow to define the square (rect.isSuare), calculate perimeter (ect.perimeter()) and area (rect.area()), swap two sides (rect.replaceSides()).

Excersise 2
---

During doing this excersise 4 classes were created: Employee, Manager and SalesMan (both inharit Employee class) and Company. Company instance holds array of employees in company. Employee instance initialized with name and salary. Information abot money I decided to keep in integer values to avoid incorrect representation.