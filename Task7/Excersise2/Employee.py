"""
"Using floating-point numbers to represent money is almost a crime"
                                                            Robert Martin
"""


class Employee:
    _name = ''
    _salary : int
    _bonus = 0
    @property
    def name(self):
        return self._name

    @property
    def salary(self):
        return self._salary

    @property
    def bonus(self):
        return self._bonus

    def __init__(self, name: str, salary: float):
        self._name = name
        self._salary = int(salary * 100)

    def setBonus(self, bonus : int):
        self._bonus = bonus * 100

    def toPay (self) -> float:
        return (self._salary + self._bonus) / 100

class SalesPerson(Employee):
    __persent : int
    def __init__(self, name : str, salary : float, persent : int):
        self._name = name
        self._salary = int(salary * 100)
        self.__persent = persent

    def setBonus(self):
        if self.__persent > 100:
            self._bonus *= 2
        elif self.__persent > 200:
            self._bonus *= 3

class Manager(Employee):
    __quantity : int
    def __init__(self, name : str, salary : float, clientAmount : int):
        self._name = name
        self._salary = int(salary * 100)
        self.__quantity = clientAmount

    def setBonus(self):
        if self.__quantity > 100:
            self._bonus += 100
        elif self.__quantity > 150:
            self._bonus += 500

