Task 12
=== 

Write and execute commands. Collect the output. 

![Иллюстрация к проекту](img/Task1.jpg)

ls is UNIX tool that shows he list of files in current folder. Sytacsis (inline)'ls [options] [files]'.


 (inline)'ls -|/' will ouyput 'cannot access '-': No such file or directory' because we didn`t create directory with lable -. Running the (inline)'ls' and (inline)'ls ~' commands showed me test because I created this folder before. Command ls -a outputs the hidden directories. Cygwin showed the test and directories which are hidden from user such as .bash_history, .bash_profile etc. In UNIX file or directory is considered to be hidden if the name staerts on dot.  (inline)'ls -l' command displaying Unix file types, permissions, number of hard links, owner, group, size, last-modified date and filename.


 ![Иллюстрация к проекту](img/Task1b.jpg)

(inline) 'mkdir' means a 'make directory' mdash; command which creates new directory. (inline)'cd' mdash; change directory mdash; moving in the catalog, changes current directory by typing (inline)'cd ..' we return to user`s home directory. (inline)'mv' moves the file into another directory or renames it to given name. 


![Task2](img/Task2.jpg)

(inline) 'ls -l' outputs the file with preferences (rwx) whrere r mdash; reading w mdash; writing x mdash; executing.