
import sys


def change_position (players:list):
    n = len(players)-1
    c = players[n]
    players[n] = players[0]
    players[0] = c
    print(players)

if __name__ == '__main__':
    change_position(
        players = ['Asleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Preskova', 'Elina Switolina']
    )