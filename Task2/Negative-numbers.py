import sys


def count_negatives (numbers: list) -> int:
    i = iter(numbers)
    ls = [] # масив, куди запишемо чи є член numbers від'ємним

    def iter_nums():
        nonlocal ls
        try:
            num = next(i)
            ls += [num < 0 or False]
            iter_nums()
        except:
            pass

    iter_nums()

    return ls.count(True)


if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negatives(input_numbers)} negative number in list {input_numbers}')