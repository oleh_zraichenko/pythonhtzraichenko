def SumGeomElements(ax, t, alim):
    ar = []
    ar.append(ax)
    while (ax * t) > alim:
        ax *= t
        ar.append(ax)

    sum = 0
    for i in range(len(ar)):
        sum += ar[i]

    return sum


if __name__ == '__main__':
    ax = float(input('Please input the first member of arithmetic progression\n'))
    n = int(input('Please input minimal number\n'))
    t = float(input('Input the step which is beetwen 0 and 1\n'))
    print(SumGeomElements(ax, t, n))
