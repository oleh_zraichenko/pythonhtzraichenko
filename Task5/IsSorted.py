from enum import Enum


class Order(Enum):
    UNSORTED = 0
    ASCENDING = 1
    DESCENDING = 2

def is_sorted(arr):
    sorted = Order.UNSORTED
    sortacs = True
    sortdesc = True
    for i in range(len(arr)-1):
        if arr[i] > arr[i+1]:
            sortacs = False
        elif arr [i] < arr [i+1]:
            sortdesc = False

    if sortacs:
        sorted = Order.ASCENDING
    elif sortdesc:
        sorted = Order.DESCENDING

    return sorted

def transform(arr, p):
    if p == 'a':
        param = Order.ASCENDING
    elif p == 'd':
        param = Order.DESCENDING
    else:
        a = 'Please, insert correct parameter: a or d'
        return a
    sorted = False
    while not sorted:
        if is_sorted(arr) == param:
            for i in range(len(arr)):
                arr[i] += i

            sorted = True
        else:
            if param == Order.ACENDING:
                arr.sort()
            if param == Order.DESCENDING:
                arr.sort(reverse = True)

    return arr

if __name__ == '__main__':
    print('Please, write a set of integer numbers if you want\n')
    arr = list(map(int, input().split()))
    param = input('Input parameter: a for ascending, b for descending\n')
    print('Output values are', transform(arr, param))

