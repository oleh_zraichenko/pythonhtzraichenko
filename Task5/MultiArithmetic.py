def MultArithmeticElements(ax, t, n):
    ar = []
    for i in range(n):
        ar.append(ax + i * t)

    p = 1
    for i in range(n):
        p *= ar[i]
    return p



if __name__ == '__main__':
    ax = float(input('Please input the first member of arithmetic progression\n'))
    n = int(input('Please input number of members\n'))
    t = int(input('Input the step\n'))
    print(MultArithmeticElements(ax, t, n))
