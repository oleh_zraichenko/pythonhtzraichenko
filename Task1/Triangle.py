# Uses python3
import sys
import math


def trSquareGeron(a, b, c):
    hper = (a + b + c) / 2
    return math.sqrt(hper * (hper - a) * (hper - b) * (hper - c))

if __name__ == '__main__':
    a, b, c = map(float, input().split())
    print(trSquareGeron(a, b, c))
