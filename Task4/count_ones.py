def binary(dec):
    bin = ''
    while dec > 0:
        y = str(dec % 2)
        bin += y
        dec = int(dec / 2)

    return bin



if __name__ == '__main__':
    dec = int(input('Please, input your number\n'))
    print('Number of ones is', binary(dec).count('1'))
