import sys


if __name__ == '__main__':
    n = int(input('Please, input the number of rows\n'))
    m = int(input('Please, input the number of cols\n'))
    i_arr = [[float(input('Input the ' + str(i) + ' ' + str(j) + ' member of matrix\n')) for i in range(m)] for j in range(n)]
    print('Inputed matrix:')
    for i in range(n):
        for j in range(m):
            print(format(i_arr[i][j], '<4'), end = "")
        print()

    res_arr = [[0 for i in range(n)] for i in range(m)]
    for i in range(m):
        for j in range(n):
            res_arr [i][j]= i_arr [j][i]

    print('Transponced matrix:')
    for i in range(n):
        for j in range(m):
            print(format(res_arr[i][j], '<4'), end="")
        print()