#let`s try it through dynamic programming
def fibo(n):
    f = [1, 1]
    sum = 2
    for i in range(2, n + 1):
        f.append(f[i-1] + f[i-2])
        sum += f[i]
    print('The sum is', sum)
    print(f)

if __name__ == '__main__':
    n = int(input())
    fibo(n)
