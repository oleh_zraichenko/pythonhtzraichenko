import sys;


def factorial(x):
    res = 1
    while x > 0:
        res *= x
        x -= 1
    return res

if __name__ == '__main__':
    n = int(input("Input the number\n"))
    print(factorial(n))

