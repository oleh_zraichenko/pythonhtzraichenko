Task 8
===

This task contains 6 excersies. The folder does not contain any source code file because task was formulated like "To think abot what happens in this code".


Excersise 1
---

In this code user can type the number.

 
-If the number is integer and 8 or bigger, script returns 0. 
```
Enter the number: 8
```
-If input number less than 8, AssertionError will be called.
```
Enter the number: 5

Traceback (most recent call last):

  File "B:\assert.py", line 3, in <module>
    assert a > b, 'Not enough'

AssertionError: Not enough
```
-If the user is trying to inpput not an iteger number (floating-point, symbols ect), ValueError will be called.
```
Enter the number: 4.7
Traceback (most recent call last):

  File "B:\assert.py", line 1, in <module>
    a = int(input('Enter the number: '))

ValueError: invalid literal for int() with base 10: '4.7'
```


Excersise 2
---

NameError will be called. 
```
NameError: name 'bar' is not defined
``` 


Excersise 3
---

Without reference to excistance of the file '/tmp/logs.txt' does not excist, the program will print 2 to console.


Excersise 4
---

SyntaxError in line 6 will be called.
```
else:
    ^
SyntaxError: invalid syntax
```

Excersise 5
---

This script will type "Error has not occured" to console.

Excersise 6
---
This script will ask user to input the filename and try to open file with given name to read until it will be stoped by user or by power outage.




