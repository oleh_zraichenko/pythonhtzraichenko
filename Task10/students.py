# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 12:33:57 2020

@author: catri
"""
import csv


def get_top_performers(file_path, number_of_top_students = 5):
    st_dict = {}
    with open(file_path, 'r') as st:
        file_reader = csv.DictReader(st, delimiter =',')
        for line in file_reader:
            st_dict.update({line['student name']: float(line['average mark'])})
            
    #print(st_dict)
    sort_orders = sorted (st_dict.items(), key=lambda x: x[1], reverse=True)
    
    #print(sort_orders)
    for i in range(5):
        print(sort_orders[i])
        

def write_to_new(file_path_old, file_path_new):
    st_lst = []
    with open(file_path_old, 'r') as st:
        file_reader = csv.DictReader(st, delimiter =',')
        for line in file_reader:
            st_lst.append([line['student name'], int(line['age']), float(line['average mark'])])
        
    st_lst.sort(key = lambda x: x[1], reverse = True)
        
    for account in st_lst:
        print(account)  
        
        
    with open(file_path_new, 'w') as stSorted:
        columns = ['student name', 'age', 'average mark']
        writer = csv.writer(stSorted)
        writer.writerow(columns)
        writer.writerows(st_lst)
       
        

    #if __name__ == '__main':         
        #get_top_performers('data/students.csv')
write_to_new('data/students.csv', 'data/students_sorted.csv')