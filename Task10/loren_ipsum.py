def count_words(s: list):
    dict = {}
    for word in s:
        dict[word] = s.count(word)
        
    return dict

if __name__ == '__main__':
    words = []
    try:
        f = open('data/lorem_ipsum.txt')
        words = f.read().split(' ')
        f.close()
        
    except FileNotFoundError:
        print('Cannot find the file, sorry (-_-)')

    worddict = count_words(words)
    print(max(worddict, key = lambda x: worddict[x]))
    