Task 10
===

This task has 3 excersises.


Excersise 1
---

I wrote a script which opens a file 'unsorted_names.txt', takes a list of names, sorts it and writes sorted list to sorted_names.txt.


Excersise 2
---

Implement a function which search for most common words in the file 'lorem_ipsum.txt'.


Excresise 3 
---

In this excersise we worked with 'student.csv' file. It contains two parts.

1. Implement a function which receives file path and returns names of top performer students.
2. Implement a function which receives the file path with students info and writes CSV student information to the new file in descending order of age. 